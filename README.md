# uRest

A small library to implement small rest services

## Getting started

This is how you can create a simple endpoint for the GET http method and the path:
/api/v1/greet/mawels

```
import URest from 'urest';

const rest = new URest();

rest.addEndpoint("/api/v1", "GET", "/greet/:name", (req, res) => {
    let name = rest.getParam(req, "name");
    if (!name) {
        rest.handleInternalError(req, res);
        return;
    }

    res.writeHead(200);
    res.write(`Hello ${name}`);
    res.end();    
});

rest.listen(8090);
```

The service will output the text "Hello mawels"
