"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const http_1 = __importDefault(require("http"));
class URest {
    constructor() {
        this.contexts = [];
        this.handleNotFound = (req, res) => {
            res.setHeader("Content-Type", "application/json");
            res.writeHead(404);
            res.write(JSON.stringify({
                code: 404,
                error: "Not found"
            }));
            res.end();
        };
        this.handleForbidden = (req, res) => {
            res.setHeader("Content-Type", "application/json");
            res.writeHead(403);
            res.write(JSON.stringify({
                code: 403,
                error: "Forbidden"
            }));
            res.end();
        };
        this.handleInternalError = (req, res) => {
            res.setHeader("Content-Type", "application/json");
            res.writeHead(500);
            res.write(JSON.stringify({
                code: 500,
                error: "Internal server error"
            }));
            res.end();
        };
    }
    getContextByRequest(req) {
        let matchingContexts = this.contexts.filter(c => req.url.includes(c.name));
        if (!matchingContexts || matchingContexts.length == 0) {
            return;
        }
        else if (matchingContexts.length == 1) {
            return matchingContexts[0];
        }
        ;
        let maxLength = matchingContexts[0].name.length;
        let matchIndex = 0;
        matchingContexts.forEach((context, index) => {
            if (context.name.length > maxLength) {
                maxLength = context.name.length;
                matchIndex = index;
            }
        });
        return matchingContexts[matchIndex];
    }
    getContext(name) {
        return this.contexts.find(c => c.name == name);
    }
    isParameter(value) {
        let matches = value.match(/:[a-z|0-9]+/i);
        return matches && matches.length == 1 ? true : false;
    }
    validatePath(requestPath, routePath) {
        if (requestPath == '/')
            return requestPath == routePath;
        let isValid = true;
        let rtParts = routePath.split('/');
        let reqParts = requestPath.split('/');
        if (reqParts.length != rtParts.length)
            return false;
        rtParts.forEach((part, i) => {
            if (!this.isParameter(part) && part != reqParts[i]) {
                isValid = false;
            }
        });
        return isValid;
    }
    routeMatches(context, route, req) {
        return req.method == route.method && this.validatePath(req.url, context.name + route.path);
    }
    getRoute(context, req) {
        return context.routes.find(r => this.routeMatches(context, r, req));
    }
    router(req, res) {
        let context = this.getContextByRequest(req);
        if (!context) {
            res.statusCode = 404;
            console.log(req.socket.remoteAddress, '[', new Date(), ']', `"${req.method} ${req.url} HTTP/${req.httpVersion}" ${res.statusCode} - context not found`);
            this.handleNotFound(req, res);
            return;
        }
        let route = this.getRoute(context, req);
        if (!route) {
            res.statusCode = 404;
            console.log(req.socket.remoteAddress, '[', new Date(), ']', `"${req.method} ${req.url} HTTP/${req.httpVersion}" ${res.statusCode} - resolved to context: '${context.name}' method not found`);
            this.handleNotFound(req, res);
            return;
        }
        route.exec(req, res);
        console.log(req.socket.remoteAddress, '[', new Date(), ']', `"${req.method} ${req.url} HTTP/${req.httpVersion}" ${res.statusCode} - resolved to context: '${context.name}' method: '${route.path}'`);
    }
    getParam(req, name) {
        let context = this.getContextByRequest(req);
        let route = this.getRoute(context, req);
        let rtParts = (context.name + route.path).split('/');
        let reqParts = req.url.split('/');
        let result;
        rtParts.forEach((part, index) => {
            if (this.isParameter(part) && part == ':' + name) {
                result = reqParts[index];
            }
        });
        return result;
    }
    addEndpoint(contextName, method, endpointName, handler) {
        let context = this.getContext(contextName);
        if (!context) {
            context = {
                name: contextName,
                routes: []
            };
            this.contexts.push(context);
        }
        context.routes.push({
            method: method,
            path: endpointName,
            exec: handler
        });
    }
    listen(port) {
        let listener = (req, res) => this.router(req, res);
        http_1.default.createServer(listener).listen(port);
    }
}
exports.default = URest;
