/// <reference types="node" />
import { IncomingMessage, ServerResponse } from 'http';
export declare type EndpointHandler = (request: IncomingMessage, response: ServerResponse) => void;
export interface Route {
    method: string;
    path: string;
    exec: EndpointHandler;
}
export interface Context {
    name: string;
    routes: Route[];
}
export default class URest {
    private contexts;
    constructor();
    handleNotFound: (req: IncomingMessage, res: ServerResponse) => void;
    handleForbidden: (req: IncomingMessage, res: ServerResponse) => void;
    handleInternalError: (req: IncomingMessage, res: ServerResponse) => void;
    private getContextByRequest;
    private getContext;
    private isParameter;
    private validatePath;
    private routeMatches;
    private getRoute;
    private router;
    getParam(req: IncomingMessage, name: string): string | undefined;
    addEndpoint(contextName: string, method: string, endpointName: string, handler: EndpointHandler): void;
    listen(port: number): void;
}
