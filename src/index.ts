import http, { IncomingMessage, ServerResponse } from 'http';
export type EndpointHandler = (request: IncomingMessage, response: ServerResponse) => void;

export interface Route {
    method: string,
    path: string,
    exec: EndpointHandler
}

export interface Context {
    name: string,
    routes: Route[]
}

export default class URest {
    private contexts: Context[] = [];

    constructor() {}

    public handleNotFound = (req: IncomingMessage, res: ServerResponse) => {
        res.setHeader("Content-Type", "application/json");
        res.writeHead(404);
        res.write(JSON.stringify({
            code: 404,
            error: "Not found"
        }));
        res.end();
    }

    public handleForbidden = (req: IncomingMessage, res: ServerResponse) => {
        res.setHeader("Content-Type", "application/json");
        res.writeHead(403);
        res.write(JSON.stringify({
            code: 403,
            error: "Forbidden"
        }));
        res.end();
    }

    public handleInternalError = (req: IncomingMessage, res: ServerResponse) => {
        res.setHeader("Content-Type", "application/json");
        res.writeHead(500);
        res.write(JSON.stringify({
            code: 500,
            error: "Internal server error"
        }));
        res.end();
    }

    private getContextByRequest(req: IncomingMessage) {
        let matchingContexts = this.contexts.filter(c => req.url!.includes(c.name));
        if (!matchingContexts || matchingContexts.length == 0) {
            return;
        } else if (matchingContexts.length == 1) {
            return matchingContexts[0];
        };

        let maxLength = matchingContexts[0].name.length;
        let matchIndex = 0;
        matchingContexts.forEach((context, index) => {
            if (context.name.length > maxLength) {
                maxLength = context.name.length;
                matchIndex = index;
            }
        });

        return matchingContexts[matchIndex];
    }

    private getContext(name: string) {
        return this.contexts.find(c => c.name == name);
    }

    private isParameter(value: string) {
        let matches = value.match(/:[a-z|0-9]+/i);
        return matches && matches.length == 1 ? true : false;
    }

    private validatePath(requestPath: string, routePath: string) {
        if (requestPath == '/') return requestPath == routePath;
        
        let isValid = true;
        let rtParts = routePath.split('/');
        let reqParts = requestPath.split('/');

        if (reqParts.length != rtParts.length) return false;

        rtParts.forEach((part, i) => {
            if (!this.isParameter(part) && part != reqParts[i]) {
                isValid = false;
            }
        });

        return isValid;
    }

    private routeMatches(context: Context, route: Route, req: IncomingMessage) {
        return req.method == route.method && this.validatePath(req.url!, context.name + route.path)
    }

    private getRoute(context: Context, req: IncomingMessage) {
        return context.routes.find(r => this.routeMatches(context, r, req));
    }

    private router(req: IncomingMessage, res: ServerResponse) {
        let context = this.getContextByRequest(req);
        if (!context) {
            res.statusCode = 404;
            console.log(req.socket.remoteAddress, '[', new Date(), ']', `"${req.method} ${req.url} HTTP/${req.httpVersion}" ${res.statusCode} - context not found`);
            this.handleNotFound(req, res);
            return;
        }

        let route = this.getRoute(context, req);
        if (!route) {
            res.statusCode = 404;
            console.log(req.socket.remoteAddress, '[', new Date(), ']', `"${req.method} ${req.url} HTTP/${req.httpVersion}" ${res.statusCode} - resolved to context: '${context.name}' method not found`);
            this.handleNotFound(req, res);
            return;
        }
        
        route.exec(req, res);
        console.log(req.socket.remoteAddress, '[', new Date(), ']', `"${req.method} ${req.url} HTTP/${req.httpVersion}" ${res.statusCode} - resolved to context: '${context.name}' method: '${route.path}'`);
    }

    public getParam(req: IncomingMessage, name: string): string | undefined {
        let context = this.getContextByRequest(req);
        let route = this.getRoute(context!, req);
        let rtParts = (context!.name + route!.path).split('/');
        let reqParts = req.url!.split('/');

        let result;
        rtParts.forEach((part, index) => {
            if (this.isParameter(part) && part == ':' + name) {
                result = reqParts[index];
            }
        });

        return result;
    }

    public addEndpoint(contextName: string, method: string, endpointName: string, handler: EndpointHandler) {
        let context = this.getContext(contextName);

        if (!context) {
            context = {
                name: contextName,
                routes: []
            }

            this.contexts.push(context);
        }

        context.routes.push({
            method: method,
            path: endpointName,
            exec: handler
        });    
    }

    public listen(port: number) {
        let listener = (req: IncomingMessage, res: ServerResponse) => this.router(req,res);
        http.createServer(listener).listen(port)
    }
}